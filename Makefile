# Top section copied from http://clarkgrubb.com/makefile-style-guide
MAKEFLAGS += --warn-undefined-variables
SHELL := bash
.SHELLFLAGS := -o errexit -o nounset -o pipefail -c
.DEFAULT_GOAL := run
.DELETE_ON_ERROR:
.SUFFIXES:


# STATIC VARS
PROJ_DIR := $(PWD)
PROJ_NAME := marwamc/images/etcd
CONTAINER_NAME := etcd
IMAGE_TAG := latest-rc
REGISTRY_URL := registry.gitlab.com
REGISTRY_USER := $(GITLAB_USERNAME)
REGISTRY_TOKEN := $(GITLAB_TOKEN)


secrets:
	export $(grep -v '^#' .env | xargs)

# DOCKER TARGETS -----------------------------------------------------------------------
docker-build:
	docker build -t ${REGISTRY_URL}/${PROJ_NAME}:${IMAGE_TAG} .

docker-run: docker-build
	docker run -it --rm \
		--env ETC_DATA_DIR=/home/moran/etc-data \
		--name ${CONTAINER_NAME} \
		--publish 2379:2379 \
		--publish 2380:2380 \
		--user 1000:1000 \
		--volume $(PWD)/etcd-data.tmp:/home/moran/etcd-data:rw \
		${REGISTRY_URL}/${PROJ_NAME}:${IMAGE_TAG}

docker-shell: docker-build
	docker run -it --rm \
		--entrypoint /bin/bash \
		--name ${CONTAINER_NAME} \
		--user 1000:1000 \
		--volume $(PWD)/etcd-data.tmp:/etcd-data:rw \
		${REGISTRY_URL}/${PROJ_NAME}:${IMAGE_TAG}

registry-login:
	@docker login -u $(REGISTRY_USER) -p $(REGISTRY_TOKEN) $(REGISTRY_URL)

publish: build registry-login
	docker push ${REGISTRY_URL}/${PROJ_NAME}:${IMAGE_TAG}

stop:
	-docker stop ${CONTAINER_NAME}
	-docker rm ${CONTAINER_NAME}

show:
	docker ps -a --format 'table {{.Names}}\t{{.Status}}\t{{.Ports}}'

shell:
	docker exec -it ${CONTAINER_NAME} /bin/bash

cleanup:
	-docker stop -vf $(shell docker ps -a -q)
	-docker rm -vf $(shell docker ps -a -q)
	-docker rmi -f $(shell docker images -a -q)
