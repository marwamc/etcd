# etcd
[![pipeline status](https://gitlab.com/marwamc/etcd/badges/main/pipeline.svg)](https://gitlab.com/marwamc/etcd/-/commits/main)

Herein we give [etcd][] a spin.

> etcd is a strongly consistent, distributed key-value store that 
> provides a reliable way to store data that needs to be accessed 
> by a distributed system or cluster of machines


## pre-requisites
The following libraries/packages are required to run this:

1. [docker desktop][]
2. [gnu make][] (can be side-stepped if you want to run the commands without make recipes)
3. [etcdctl][] should be installed on your local machine - (to interact with the remote etcd instance)


## details
In this setup we install etcd on an ubuntu focal image. 
This choice is driven by familiarity (with ubuntu focal) - 
in future will consider a smaller base image.

## tasks
- [x] 1. init base image 
- [x] 2. install etcd test version
- [x] 3. run single node etcd server in docker
- [ ] 4. gitlab ci build and deploy image
- [ ] 5. setup controlled access
- [ ] 6. run a 3 node cluster in docker following instructions [here][] 


## references
1. [etcd home][etcd]
2. [confd][]


[docker desktop]: https://www.docker.com/products/docker-desktop/
[confd]: https://github.com/kelseyhightower/confd
[etcd]: https://etcd.io
[etcdctl]: https://github.com/etcd-io/etcd/releases/
[gnu make]: https://www.gnu.org/software/make/
[here]: https://etcd.io/docs/v3.5/tutorials/how-to-setup-cluster/
