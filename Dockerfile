ARG CI_REGISTRY=registry.gitlab.com
ARG BASE_IMAGE_NAME=marwamc/images/ubuntu-base
ARG BASE_IMAGE_TAG=focal-latest

FROM ${CI_REGISTRY}/${BASE_IMAGE_NAME}:${BASE_IMAGE_TAG}

# install etcd
ENV ETCD_VER=v3.5.3
ENV DOWNLOAD_URL=https://github.com/etcd-io/etcd/releases/download

RUN set -ex \
    && curl -L ${DOWNLOAD_URL}/${ETCD_VER}/etcd-${ETCD_VER}-linux-amd64.tar.gz \
        -o /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz \
    && tar xzvf /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz \
        -C /usr/local/bin \
        --strip-components=1 \
    && rm -f /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz

COPY 'run-etcd' /usr/local/bin/

EXPOSE 2379 2380

ENTRYPOINT ["/bin/bash"]
CMD ["run-etcd"]
